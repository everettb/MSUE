$(document).ready(function() {
    // google map api for gettng lat/long of event
    var information = document.getElementById('information');
    var latitude = 42.7369792;
    var longitude = -84.4838654;
    var map = null;
    var mapButton = document.getElementById('mapOpenButton');
    var mapCanvas = document.getElementById('map-canvas');
    var mapInput;
    var marker = null;

    // Create Google Maps search input
    var input = document.getElementById('search_input');
    var searchBox = new google.maps.places.SearchBox(input);
    google.maps.event.addListener(searchBox, 'places_changed', function() {
        var places = searchBox.getPlaces()[0];
        updateInformation(places.geometry.location, places.formatted_address);
        if (map) {
            placeMarker(places.geometry.location);
            mapInput = places.formatted_address;
        }
    });

    // Getting user position
    navigator.geolocation.getCurrentPosition(updateStartPosition);

    // If we get user position we set default lat ang long
    function updateStartPosition(location) {
      if (location.coords) {
          latitude = location.coords.latitude;
          longitude = location.coords.longitude;
      }
    }

    mapButton.addEventListener('click', function() {
        $(".map-canvas").css("height", "500px");
        if (!map) {
            loadMap();
        } else {
            $('#map-dialog').show();
        }
    });

    function openMap() {
        if (!map) {
            loadMap();
        } else {
            $('#map-dialog').show();
        }
    }

    function loadMap() {
      // Coordinates to center the map
      var myLatlng = new google.maps.LatLng(latitude, longitude);

      // Other options for the map, pretty much selfexplanatory
      var mapOptions = {
          zoom: 14,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      // Create search box for the map
      mapInput = document.createElement('input');
      mapInput.className = 'search-box';
      mapInput.placeholder = 'Search for a places';
      var mapSearchBox = new google.maps.places.SearchBox(mapInput);

      google.maps.event.addListener(mapSearchBox, 'places_changed', function() {
        var places = mapSearchBox.getPlaces()[0];
        updateInformation(places.geometry.location, places.formatted_address);
        placeMarker(places.geometry.location);
      });

      // Attach a map to the DOM Element, with the defined settings
      map = new google.maps.Map(mapCanvas, mapOptions);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(mapInput);

      marker = new google.maps.Marker({
        map: map
      });

      google.maps.event.addListener(map, 'click', function(e) {
        placeMarker(e.latLng);
        updateInformation(e.latLng);
      });

      $('#map-dialog').show();

      if ($('#latlong').val()) {
        var coords = $('#latlong').val().split(',');
        var location = new google.maps.LatLng(coords[0], coords[1]);
        placeMarker(location);
      }
    }

    function updateInformation(location, address) {
      var content = '<p>';
      if (address) {
        content += '<b>' + address + '</b><br/>';
        $('#search_input').val(address);
      } else {
        $('#search_input').val('');
      }
      content += 'Latitude: ' + location.lat() + '<br/> Longitude: ' + location.lng();
      content += '</p>';
      information.innerHTML = content;

      // Save the data in the field
      $('#latlong').val(location.lat() + ',' + location.lng());
    }

    function placeMarker(location) {
      marker.setPosition(location);
      map.setCenter(location);
    }

    if ($('#latlong').val()) {
        console.log($("#latlong").val());
      var coords = $('#latlong').val().split(',');
      var location = new google.maps.LatLng(coords[0], coords[1]);
      updateInformation(location);
    }
    // end google map api for gettng lat/long of event
});

$(document).ready(function() {
    // get hostname for the ajax call
    var hostName = $(location).attr('hostname');

    // validate reg link url
    $("#link").focusout(function(){
        if(/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("#link").val())){
            console.log("valid URL");
        } else {
            $('html, body').animate({
                scrollTop: ($('.response').offset().top)
            },500);
            $(".response").append("<h3 style='color:red; margin-top:80px;'>Registration Information: Please enter a valid url</h3>");
        }
    });

    // attempt to fill in url title
    $("#title").change(function(){
        var $titleString = $("#title").val();
        // Replace unwanted characters
        $titleString = $titleString.replace(/^\s+|\s+$/g,""); // Trim whitespace from both ends
        $titleString = $titleString.replace(/[^a-zA-Z 0-9_]+/g,' '); // Convert bad characters to space
        $titleString = $titleString.replace(/\s/g, "-"); // Convert spaces to hyphens

        // Remove consecutive hyphens
        while ($titleString.indexOf("--") > -1) {
            $titleString = $titleString.replace("--",'-');
        }

        // Trim hyphen from beginning
        while ($titleString.indexOf("-") == 0) {
            $titleString = $titleString.substring(1, $titleString.length);
        }

        // Trim hyphen from end
        while ($titleString.lastIndexOf("-") == $titleString.length -1 ) {
            $titleString = $titleString.substring(0,$titleString.length -1);
        }

        // Lowercase
        $titleString = $titleString.toLowerCase();

        // check to see if urlTitle exists
        $.ajax({
            url:"https://"+hostName+"/api/content/query/+contentType:calendarEvent%20+calendarEvent.urlTitle:"+$titleString+"*/orderby/calendarEvent.urlTitle%20asc/limit/100",
            dataType:'json',
            type:'get',
            cache: false,
            success: function(data){
                console.log(data);
                var counter = 1;
                var $newUrl = $titleString;
                $(data.contentlets).each(function(index, value){
                    console.log("Original urlTitle: "+value.urlTitle);
                    // if urlTitle exists increment urlTitle
                    if(value.urlTitle == $newUrl){
                        console.log("A match exists");
                        $newUrl = $titleString+"-"+counter;
                        console.log("New urlTitle: "+$newUrl);
                        counter++;
                    }
                });
                console.log("Final urlTitle: "+$newUrl);
                //map title to urlTitle
                $("#urlTitle").val($newUrl);
            }
        });
    });

    $("#msueEventSubmission").submit(function(e) {
        e.preventDefault();
        // set location event relationship
        if($("#LocationEvent").val()!= undefined && $("#LocationEvent").val().length > 0) {
            var idLE = $("#LocationEvent").val().toString();
        }
        /*
        for the following Relationships: Organization, Dataset & Person:
        first set an array to dump all of the selected checkboxes into
        then if the array has any length set the relationship var to be used in the dataObject below
        */
        var idOEarray = [];
        $("input[name='OrganizationEvent']:checked").each(function(){
            idOEarray.push($(this).val());
        });

        if(idOEarray.length > 0){
            var idOE = idOEarray.toString().replace(/,/g,'\" \"');
        }

        // these 3 below get combined into 1 array for the dataset relationship
        var idDEarray = [];
        $("input[name='DatasetcalendarEventTopic']:checked").each(function(){
            idDEarray.push($(this).val());
        });

        $("input[name='DatasetcalendarEventProgram']:checked").each(function(){
            idDEarray.push($(this).val());
        });

        if($("#4hSites").val()!= undefined && $("#4hSites").val().length > 0) {
            idDEarray.push($("#4hSites").val());
        }

        if(idDEarray.length > 0){
            var idDE = idDEarray.toString().replace(/,/g,'\" \"');
        }

        var idPOCarray = [];
        $("input[name='PointOfContact']:checked").each(function(){
            idPOCarray.push($(this).val());
        });

        if(idPOCarray.length > 0){
            var idPOC = idPOCarray.toString().replace(/,/g,'\" \"');
        }

        var dataObject={
            'ipAddress': '$request.getRemoteAddr()',
            'stName':'calendarEvent',
            'Location-Event': '+structureName:Place +identifier:(\"'+ idLE +'\")',
            'Organization-Event':'+structureName:Organization +identifier:(\"'+ idOE +'\")',
            'Dataset-calendarEvent':'+structureName:Dataset +identifier:(\"'+ idDE +'\")',
            'Point-OfContact':'+structureName:Person +identifier:(\"'+ idPOC +'\")'
        };
        // maps the rest of the form fields into the dataObject
        if($("#host1").val()!= undefined && $("#host1").val().length > 0){
            dataObject.host1 = $("#host1").val();
        }
        if($("#title").val()!= undefined && $("#title").val().length > 0){
            dataObject.title = $("#title").val();
        }

        if($("#urlTitle").val()!= undefined && $("#urlTitle").val().length > 0) {
            dataObject.urlTitle = $("#urlTitle").val();
        }

        if($("#summary").val()!= undefined && $("#summary").val().length > 0){
            dataObject.summary = $("#summary").val();
        }

        if($("#startDateDate").val()!= undefined && $("#startDateDate").val().length > 0 && $("#startDateTime").val()!= undefined && $("#startDateTime").val().length > 0){
            dataObject.startDate = $("#startDateDate").val() +" "+ $("#startDateTime").val();
        }

        if($("#time").val()!= undefined && $("#time").val().length > 0) {
            dataObject.time = $("#time").val();
        }

        if($("#endDateDate").val()!= undefined && $("#endDateDate").val().length > 0 && $("#endDateTime").val()!= undefined && $("#endDateTime").val().length > 0){
            dataObject.endDate = $("#endDateDate").val() +" "+ $("#endDateTime").val();

            // set expiration date 30 days from end date
            var date = new Date($("#endDateDate").val());
            var newDate = new Date(date);
            newDate.setDate(newDate.getDate()+30);
            var options = { year: 'numeric', month: '2-digit', day: 'numeric' };
            // ko-KR outputs to this format 2012. 12. 20.  so replace to fix
            var expire = newDate.toLocaleDateString("ko-KR",options).replace(/. /g, "-").replace(".","");
            dataObject.expirationDate = expire +" "+ $("#endDateTime").val();
        }

        if($("#place").val()!= undefined && $("#place").val().length > 0) {
            dataObject.place = $("#place").val();
        }

        if($("#contactInfo").val()!= undefined && $("#contactInfo").val().length > 0) {
            dataObject.contactInfo = $("#contactInfo").val();
        }

        if($("#description").val()!= undefined && $("#description").val().length > 0) {
            dataObject.description = $("#description").val();
        }

        if($("#link").val()!= undefined && $("#link").val().length > 0) {
            dataObject.link = $("#link").val();
        }

        if($("#countySelector").val()!= undefined && $("#countySelector").val().length > 0){
            dataObject.countySelector = $("#countySelector").val();
            console.log("counties: "+ dataObject.countySelector);
        }

        if($("#tags").val()!= undefined && $("#tags").val().length > 0) {
            dataObject.tags = $("#tags").val();
        }

        // testing out latlong dataObject.latlong = "42,-84";
        if($("#latlong").val()!= undefined && $("#latlong").val().length > 0) {
            dataObject.latlong = $("#latlong").val();
            console.log("latlong val:"+ $("#latlong").val());
            console.log(dataObject.latlong);
        }

        // change to first tab upon submit to see errors more easily
        // 3rd tab
        $(".tabs-title").removeClass("is-active");
        $("#panel3d-label").attr("aria-selected","false");
        // 3rd panel
        $("#panel3d").removeClass("is-active");
        $("#panel3d").attr("aria-hidden","true");
        // 1st tab
        $(".tabs .tabs-title:first-child").addClass("is-active");
        $("#panel1d-label").attr("aria-selected","true");
        // 1st panel
        $("#panel1d").addClass("is-active");
        $("#panel1d").attr("aria-hidden", "false");

        $.ajax({
            url: 'https://'+hostName+'/api/content/publish/1/',
            type: 'POST',
            cache: false,
            data: dataObject,
            beforeSend: function (request){
                var username = $("#username").val();
                var password = $("#password").val();
                // This sends the user who authenticates against the dotCMS server
                request.setRequestHeader("DOTAUTH", window.btoa(username +":"+ password));
            },
            success: function(data, status, xhr) {
                console.log("submitted");
                var a = xhr.getAllResponseHeaders();
                console.log(a.substring(a.indexOf('identifier: ') + 'identifier: '.length , a.indexOf('\n', a.indexOf('identifier: ') + 'identifier: '.length)));
                $('#msueEventSubmission').each(function(){
                    this.reset();
                });
                $('html, body').animate({
                    scrollTop: ($('.response').offset().top)
                },500);
                $(".response").append("<h3 style=' margin-top:80px;'>Your event has been submitted</h3>");
            },
            error: function(data,status,xhr){
                console.log(data);
                $('html, body').animate({
                    scrollTop: ($('.response').offset().top)
                },500);
                // user tries to save to folder which they don't have permission to
                if(data.responseText.indexOf("404 Not Found") > 0){
                    $(".response").append("<h3 style='color:red; margin-top:80px;'>Either an event is already in the system with the same URL Title or you have selected a website folder you do not have access to. If you believe you have selected a folder you do have access to, please Try changing the name of the URL Title, or clear your browser cache and resubmit your event.</h3>");
                }
                else {
                    $(".response").append("<h3 style='color:red; margin-top:80px;'>"+data.responseText+"</h3>");
                }

            },
        }); // end ajax
    });
});
